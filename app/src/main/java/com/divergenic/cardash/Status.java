package com.divergenic.cardash;

/**
 * Created by jaishankar on 17/5/15.
 */
public enum Status {
    CREATED,
    READ_PREF_VALUES,
    INITIALISED,
    NETWORKGPSOK,
    SCAN_STARTED,
    FOUND_FIRST_LOCATION,
    SAVED_USER,
    SCAN_RUNNING,
    SCAN_STOPPED,
    LOADING_CARS,
    LOADED_CARS;

}
