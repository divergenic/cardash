package com.divergenic.cardash;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.parse.CountCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;



public class SplashScreenActivity extends ActionBarActivity implements CountCallback {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
               // try {




                ParseUser currentUser = ParseUser.getCurrentUser();
                    ParseRelation rel = currentUser.getRelation("car");
                    if (rel != null) {
                        Log.i("Car Dash", "relation exists");
                        ParseQuery query = rel.getQuery();
                        query.countInBackground(SplashScreenActivity.this);
                    } else {
                        Log.i("Car Dash", "No Car relations defined");
                    }
            }
        }, SPLASH_TIME_OUT);
    }


    @Override
    public void done(int i, ParseException e) {
        Log.i("Car Dash", "num cars = "+i);

        Intent intent = null;
        int numCars = i;


        if (numCars <= 0) {
            Log.i("Car Dash", "No cars registered...");
            // Start registration activity
            intent = new Intent(SplashScreenActivity.this, RegistrationActivity.class);

        } else {
            Log.i("Car Dash", "A car was registered...");

            // This method will be executed once the timer is over
            // Start your app main activity
            intent = new Intent(SplashScreenActivity.this, DashboardActivity.class);
        }
        startActivity(intent);

        // close this activity
        finish();

    }
}
