package com.divergenic.cardash;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;


public class DashboardActivity extends ActionBarActivity {

    private TextView lblNetwork;
    private TextView lblGPS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.action_bar_layout, null);
        lblNetwork = (TextView)v.findViewById(R.id.lbl_network);
        lblGPS = (TextView)v.findViewById(R.id.lbl_gps);
        ab.setCustomView(v);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment(lblNetwork, lblGPS))
                    .commit();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent=new Intent(DashboardActivity.this, LevelSettings.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private DecimalFormat df = new DecimalFormat("0");


        private RadarView radar;
        private VideoView leftVideoView;
        private VideoView rightVideoView;

        private ListView listView;

        private TextView lblNetwork;
        private TextView lblGPS;
        private TextView lblSpeed;
        private TextView lblStatus;
        private TextView lblLatLong;
        private TextView lblSafetyRadius;
        private TextView radiusCars;

        private Handler handler;

        private boolean refreshView = false;
        private boolean userdenied;

        private MyCar myCar;

        private boolean playingWarning = false;
        CustomAdapter customAdapter;

        public PlaceholderFragment(TextView lblNetwork, TextView lblGPS) {
            this.lblNetwork = lblNetwork;
            this.lblGPS = lblGPS;
        }

        @Override
        public void onDestroy()
        {
            super.onDestroy();
            if(myCar != null)
            {
                myCar.resetSpeed();
            }

        }

        @Override
        public  void onStop()
        {
            super.onStop();
            //myCar.destroyUserCar();
        }

        @Override
        public void onConfigurationChanged(Configuration newConfig) {
            super.onConfigurationChanged(newConfig);

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

            radar = (RadarView) rootView.findViewById(R.id.radarView);
            leftVideoView = (VideoView) rootView.findViewById(R.id.leftVideoView);
            rightVideoView = (VideoView) rootView.findViewById(R.id.rightVideoView);

            listView = (ListView) rootView.findViewById(R.id.carList);
//            lblNetwork = (TextView) rootView.findViewById(R.id.lbl_network);
//            lblGPS = (TextView) rootView.findViewById(R.id.lbl_gps);
            lblStatus = (TextView) rootView.findViewById(R.id.lbl_status);
            lblSpeed = (TextView) rootView.findViewById(R.id.lbl_speed);
            lblLatLong = (TextView) rootView.findViewById(R.id.lbl_latlong);
            lblSafetyRadius = (TextView) rootView.findViewById(R.id.lbl_safetyRadius);
            //radiusCars = (TextView) rootView.findViewById(R.id.radiusCars);


            MediaController mediaController = new MediaController(getActivity());
            mediaController.setAnchorView(leftVideoView);
            try{
                String leftVideoPath = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.left;
                leftVideoView.setVideoURI(Uri.parse(leftVideoPath));

                String rightVideoPath = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.right;
                rightVideoView.setVideoURI(Uri.parse(rightVideoPath));
            }
            catch (Exception e){
                e.printStackTrace();
            }

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try{
                        Car car = (Car) listView.getAdapter().getItem(position);
                        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", car.getLatitude(), car.getLongitude());
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

            return rootView;
        }

        @Override
        public void onResume() {
            super.onResume();
            refreshView = true;
            startViewUpdateTimer();
        }

        @Override
        public void onPause() {
            super.onPause();
            stopViewUpdateTimer();
            if (getActivity().isFinishing()) {
//                myCar.getMyParseObject().put("online", false);
                if(myCar != null)
                {
                    myCar.saveDetails();
                }

            }
        }





        private void startViewUpdateTimer() {
            handler = new Handler();
            handler.postDelayed(refreshTask, 1000);
        }

        private void stopViewUpdateTimer() {
            refreshView = false;
        }

        public void showSettingsAlert(final String whichSetting, String title, String message){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

            // Setting Dialog Title
            alertDialog.setTitle(title);

            // Setting Dialog Message
            alertDialog.setMessage(message);

            // Setting Icon to Dialog
            //alertDialog.setIcon(R.drawable.delete);

            // On pressing Settings button
            alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int which) {
                    Intent intent = new Intent(whichSetting);
                    PlaceholderFragment.this.getActivity().startActivity(intent);
                }
            });

            // on pressing cancel button
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    userdenied = true;
                }
            });

            // Showing Alert Message
            alertDialog.show();
        }

        private final Runnable refreshTask;

        {
            refreshTask = new Runnable() {
                @Override
                public void run() {
                    //Log.i("CarDash", "running refresh view thread...");
                    if (refreshView) {

                        boolean networkOK = CarApplication.getNetworkStatus(getActivity());
                        boolean gpsOK = CarApplication.getGPSStatus(getActivity());

                        Drawable indicatorOK = getActivity().getResources().getDrawable(R.drawable.shape_blue_dot);
                        Drawable indicatorNotOkay = getActivity().getResources().getDrawable(R.drawable.shape_gray_dot);

                        if (myCar!=null) {
                            lblSpeed.setText("Speed: " + df.format(myCar.currentSpeed*3.2)+ " kmph"); // convert it to kmph before display
                            lblLatLong.setText(myCar.getLatLongDisplay());
                        }

                        if (networkOK) {
                            lblNetwork.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shape_blue_dot, 0, 0, 0);
                        } else {
                            lblNetwork.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shape_gray_dot, 0, 0, 0);
                            showSettingsAlert(Settings.ACTION_SETTINGS, "Internet Connection Required", "Please connect to the internet to continue!!");
                            refreshView = false;
                            return;
                        }

                        if (gpsOK) {
                            lblGPS.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shape_blue_dot, 0, 0, 0);
                        } else {
                            lblGPS.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shape_gray_dot, 0, 0, 0);
                            showSettingsAlert(Settings.ACTION_LOCATION_SOURCE_SETTINGS, "GPS Connection Required", "Please enable GPS to continue!!");
                            refreshView = false;
                            return;
                        }


                        if (myCar == null) {
                            myCar = MyCar.getInstance(getActivity());
                        }



                        lblStatus.setText(myCar.getCurrentAppStatus());
                        lblSafetyRadius.setText("Safe dist: " + df.format(myCar.getMySafeRadius()) + " m");
                        Set<Car> closeCars = myCar.getCloseCars();
                        int size = -1;
//                        synchronized (closeCars)
//                        {
//                            size = closeCars.size();
//                        }
//                        if(size != -1)
//                        {
//                            radiusCars.setText("Within radius \n" + size);
//                        }

                        // list
//                        final List<Car> mCloseCars = new ArrayList<>(myCar.getCloseCars());
//                        final List<Car> approachingCars = new ArrayList<>(myCar.getNearestCars());
                        final List<Car> mNearestCars = new ArrayList<>(myCar.getNearestCars());


                        radar.setCarsToBeDrawn(mNearestCars);
                        List<Car> tempCars = myCar.getNearestCars();
                        List<Car> nearestCars = null;
                        synchronized (tempCars){
                            nearestCars = new ArrayList<>(tempCars);
                        }

                        customAdapter = new CustomAdapter(getActivity(), nearestCars);
                        listView.setAdapter(customAdapter);
                        customAdapter.notifyDataSetChanged();

                        // check and play alert

                        if(myCar.getAlertType() != AlertType.NONE && !playingWarning) {

                            final VideoView currentVideoView = myCar.getAlertType() == AlertType.LEFT? leftVideoView : rightVideoView;

                            currentVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    currentVideoView.setVisibility(View.INVISIBLE);
                                    PlaceholderFragment.this.playingWarning = false;
                                    Log.i("CarDash: ", "Finished playing the track..");
                                    //Toast.makeText(getActivity(), "Finished Playing video...", Toast.LENGTH_SHORT).show();
                                    myCar.setIsAlertPlaying(false);
                                    myCar.clearAlert();
                                }
                            });
                            currentVideoView.setVisibility(View.VISIBLE);
                            currentVideoView.requestFocus();

                            myCar.setIsAlertPlaying(true);
                            currentVideoView.start();

                            playingWarning = true;

                        }



                        try {
                            PlaceholderFragment.this.handler.postDelayed(refreshTask, 500);
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("CarDash: ", "Exception in DashboardActivity ", e);
                        }
                    }
                }
            };
        }
    }
}
