package com.divergenic.cardash;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.parse.ParseObject;

/**
 * Created by dependency_injection on 3/5/15.
 */
public class Car {
    private String id;
    private double latitude;
    private double longitude;
    private Double speed;
    private double distanceFromMe;
    private ParseObject parseObject;
    private String color;
    private double coordx;
    private double coordy;
    private int safetyMargin;
    private double calculatedSafetyRadius;
    private String original_color;

    public Car(String id, double latitude, double longitude, Double speed,
               double distanceFromMe, int safetyMargin, ParseObject parseObject, String color, double coordx, double coordy) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.distanceFromMe = distanceFromMe;
        this.safetyMargin = safetyMargin;
        this.parseObject = parseObject;
        this.color = color;
        this.original_color = color;
        this.coordx = coordx;
        this.coordy = coordy;

    }

    public double getCalculatedSafetyRadius() {
        return calculatedSafetyRadius;
    }

    public void setCalculatedSafetyRadius(double calculatedSafetyRadius) {
        this.calculatedSafetyRadius = calculatedSafetyRadius;
    }

    public void resetColor()
    {
        this.color = original_color;
    }

    public int getSafetyMargin() {
        return safetyMargin;
    }

    public void setSafetyMargin(int safetyMargin) {
        this.safetyMargin = safetyMargin;
    }

    public double getCoordx() {
        return coordx;
    }

    public void setCoordx(double coordx) {
        this.coordx = coordx;
    }

    public double getCoordy() {
        return coordy;
    }

    public void setCoordy(double coordy) {
        this.coordy = coordy;
    }

    public String getColor() {
        return color;
    }

    public Drawable getDrawable(Context context) {
        String vehicleType = (String) getParseObject().get("vehicleType");
        int resourceId = 0;
        if (vehicleType == "suv") {
            resourceId = R.drawable.ic_suv;
        } else if (vehicleType == "hatchback") {
            resourceId = R.drawable.ic_hatchback;
        } else {
            resourceId = R.drawable.ic_sedan;
        }

        return context.getResources().getDrawable(resourceId);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getDistanceFromMe() {
        return distanceFromMe;
    }

    public void setDistanceFromMe(double distanceFromMe) {
        this.distanceFromMe = distanceFromMe;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ParseObject getParseObject() {
        return parseObject;
    }

    public void setParseObject(ParseObject parseObject) {
        this.parseObject = parseObject;
    }

    @Override
    public boolean equals(Object other) {
        boolean result = false;
        if (other instanceof Car) {
            Car that = (Car) other;
            result = (this.getId().equals(that.getId()) && that.getId().equals(this.getId()));
        }
        return result;
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }


    public String getName() {

        String displayName = (String) getParseObject().get("displayName");
        if(displayName != null) {
            return displayName;
        }

        return this.id;

    }


}
