package com.divergenic.cardash;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.ParseAnalytics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends ActionBarActivity {
    private MyCar myCar;
    private Timer screenRefreshTimer, networkGPSTimer;
    private RadarView myRadarView;
    private boolean userdenied = false;
    private Timer listTimer;
    View headerview;
    private static final int FAR_INTERVAL = 10000; //in millis
    private boolean appIsRunning = true;
    private TextView statusView;


    private ListView gv;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gv = (ListView) findViewById(R.id.listView);
        statusView = (TextView)findViewById(R.id.statusView);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        headerview = inflater.inflate(R.layout.header,gv,false);

        networkGPSTimer = new Timer();
        TimerTask refreshTask = new TimerTask() {
            @Override
            public void run() {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        myCar = MyCar.getInstance(MainActivity.this);
                        if(!CarApplication.checkOK(getApplicationContext())){
                            if(!CarApplication.isNetworkEnabled()) {
                                showSettingsAlert(Settings.ACTION_SETTINGS, "Internet Connection Required",  "Please connect to the internet to continue!!");
                            }else if(!CarApplication.isGPSEnabled()){
                                showSettingsAlert(Settings.ACTION_LOCATION_SOURCE_SETTINGS, "GPS Connection Required", "Please enable GPS to continue!!");
                            }
                            myCar.stopScan();
                        }
                        else {
                            myCar.startScan();
                        }
                    }
                });
            }
        };
        networkGPSTimer.schedule(refreshTask, 0, 5000);

    }



    public void publishUpdate(final String text)
    {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                statusView.setText("Status: " + text);
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();
        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        boolean isOK = CarApplication.checkOK(this);

        if(isOK)
        {
            setup();
        }

    }


    public void showSettingsAlert(final String whichSetting, String title, String message){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(whichSetting);
                MainActivity.this.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                userdenied = true;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void setup()
    {
        Log.e("setup run>>>>>>>>>>", "i was here");
        myRadarView = (RadarView) findViewById(R.id.radarView);
        myCar = MyCar.getInstance(this);
        myCar.startScan();

        if(screenRefreshTimer != null)
        {
            screenRefreshTimer.cancel();
        }
        screenRefreshTimer = new Timer();
        TimerTask refreshTask = new TimerTask() {
            @Override
            public void run() {
                //myRadarView.setCarsToBeDrawn(myCar.getCloseCars(), myCar.getNearestCars());
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        myRadarView.invalidate();
                    }
                });
            }
        };
        screenRefreshTimer.schedule(refreshTask, 0, 500);

        listTimer = new Timer();
        TimerTask myTask = new TimerTask() {
            @Override
            public void run() {
                final List<Car> mCloseCars = new ArrayList<>(myCar.getCloseCars());


                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(!mCloseCars.isEmpty())
                        {
                            CustomAdapter customAdapter = new CustomAdapter(MainActivity.this, mCloseCars);
                            gv.setAdapter(customAdapter);
                            //customAdapter.notifyDataSetChanged();

                        }
                    }
                });

            }

        };

        listTimer.schedule(myTask, 0, 500);

        gv.addHeaderView(headerview);



    }


    public void displayCloseCars()
    {
        Set<Car> mCloseCars = myCar.getCloseCars();
        Iterator<Car> i = mCloseCars.iterator();

        while(i.hasNext())
        {
            Car carObj = i.next();
            Log.e("==>", carObj.getId());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){
            case R.id.action_settings:
                Intent intent=new Intent(MainActivity.this,LevelSettings.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


}
