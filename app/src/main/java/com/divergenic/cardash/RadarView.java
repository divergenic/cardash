package com.divergenic.cardash;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by jaishankar on 5/5/15.
 */
public class RadarView extends View {
    private int size;
    public Collection<Car> carsToBeDrawn = new ArrayList<>();

    private int myCarColor = Color.parseColor("#50a0f0");

    public RadarView(Context context) {
        super(context);
    }

    public RadarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Collection<Car> getCarsToBeDrawn() {
        return carsToBeDrawn;
    }

    public void setCarsToBeDrawn(Collection<Car> nearestCars) {      //, Collection<Car> approachingCars) {
        synchronized (carsToBeDrawn){
            this.carsToBeDrawn.clear();
//            this.carsToBeDrawn.addAll(closeCars);
//            this.carsToBeDrawn.addAll(approachingCars);
            this.carsToBeDrawn.addAll(nearestCars);
            this.invalidate();
        }

    }
    private int findMin(int val1, int val2){
        return val1 < val2 ? val1 : val2;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);


        int width=getWidth();
        int height=getHeight();

        size = findMin(width,height);

        float centerx = size/2;
        float centery = size/2;

        Paint paint=new Paint();

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.parseColor("#ffffff"));
        canvas.drawCircle(centerx, centery, size / 2, paint);



        //drawTriangle(canvas, centerx, centery, 40, 60, myCarColor);
        paint.setColor(myCarColor);
        paint.setAlpha(200);
        paint.setStrokeWidth(6.0f);
        canvas.drawLine(centerx, centery - 16.0f, centerx, centery + 16.0f, paint);
        canvas.drawLine(centerx-16.0f, centery, centerx + 16.0f, centery, paint);
//        canvas.drawCircle(centerx, centery, 16.0f, paint);


        synchronized (carsToBeDrawn)
        {
            for(Car c: carsToBeDrawn)
            {
                float x = (float)-c.getCoordy(); // this is done to rotate the point by 90 deg
                float y = (float)c.getCoordx(); // this is done to rotate the point by 90 deg
                x = ((size/2)*x)/100; // scale according to the dimensions of the view..
                y = ((size/2)*y)/100;
                x = centerx + x; //adjust for center to be origin...
                y = centery - y;
                paint.setStyle(Paint.Style.FILL);
                paint.setColor(Color.parseColor(c.getColor()));
                paint.setAlpha(200);
                canvas.drawCircle(x, y, 16.0f, paint);
                //Log.i("CarDash", "x,y : "+x+","+y);
                //drawTriangle(canvas, x, y, 40, 60, Color.parseColor(c.getColor()));
            }
        }


    }



    private void drawTriangle(Canvas canvas, float centreX, float centreY, float width, float height, int fillColor) {
        Paint paint=new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(fillColor);

        Path path = new Path();
        // top center
        path.moveTo(centreX, centreY - height/2);
        // left bottom
        path.lineTo(centreX + width/2, centreY + height/2);

        // right bottom
        path.lineTo(centreX - width/2, centreY + height/2);

        path.lineTo(centreX, centreY - height/2);

        path.close();
        canvas.drawPath(path, paint);

    }
}
