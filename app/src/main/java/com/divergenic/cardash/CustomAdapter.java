package com.divergenic.cardash;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomAdapter extends ArrayAdapter<Car>
{
    private Context mContext;
    //    private List<String> mCarId = new ArrayList<String>();
//    private List<Double> mCarLatitude = new ArrayList<Double>();
//    private List<Double> mCarLongitude = new ArrayList<Double>();
//    private List<Double> mCarSpeed = new ArrayList<Double>();
    private List<Car> carsToBeDrawn;

    private LayoutInflater inflater;
    private DecimalFormat df = new DecimalFormat("0");


    public CustomAdapter(Context context, List<Car> carsToBeDrawn)
    {
        super(context,R.layout.grid_item,carsToBeDrawn);
        this.mContext = context;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.carsToBeDrawn = carsToBeDrawn;
    }

    public List<Car> getCarsToBeDrawn() {
        return carsToBeDrawn;
    }

    public void setCarsToBeDrawn(List<Car> carsToBeDrawn) {
        synchronized (carsToBeDrawn){
            this.carsToBeDrawn = new ArrayList<>(carsToBeDrawn);
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View grid;
        ViewHolder viewHolder;

        if (convertView == null) {
            grid = inflater.inflate(R.layout.grid_item, null, true);
            viewHolder = new ViewHolder();
            viewHolder.cid = (TextView) grid.findViewById(R.id.carId);
            viewHolder.cdist = (TextView) grid.findViewById(R.id.carDistance);
            viewHolder.cspeed = (TextView) grid.findViewById(R.id.carSpeed);
            viewHolder.csafeMargin = (TextView) grid.findViewById(R.id.carSafetyRadius);
            viewHolder.cIcon = (ImageView) grid.findViewById(R.id.carIcon);
            viewHolder.lblLatLong = (TextView) grid.findViewById(R.id.carLatLong);
            grid.setTag(viewHolder);
        } else {
            grid = convertView;
            viewHolder = (ViewHolder) convertView.getTag();
        }


        Car currentCar = carsToBeDrawn.get(position);

        String dist_str = df.format(currentCar.getDistanceFromMe()) + " m";
        String speed_str = df.format(currentCar.getSpeed() * 3.6) + " kmph";

        viewHolder.cid.setText(currentCar.getName());
        viewHolder.cdist.setText(dist_str);
        viewHolder.cspeed.setText(speed_str);
        viewHolder.csafeMargin.setText("Safe:\n" + df.format(currentCar.getCalculatedSafetyRadius())+" m");

        //viewHolder.lblLatLong.setText(getLatLongString(currentCar));

        viewHolder.cIcon.setImageDrawable(currentCar.getDrawable(mContext));

        grid.setBackgroundColor(Color.parseColor(currentCar.getColor()));

        return grid;
    }

    private String getLatLongString(Car currentCar) {
        StringBuilder builder = new StringBuilder();

        try {
            builder.append("Lat : ");
            builder.append(currentCar.getLatitude());
            builder.append(", Long : ");
            builder.append(currentCar.getLongitude());
        }catch (Exception e) {
            return "Lat Long Unknown?!";
        }

        return builder.toString();
    }


}

class ViewHolder {
    TextView cid;
    TextView cdist;
    TextView cspeed;
    TextView lblLatLong;
    ImageView cIcon;
    TextView csafeMargin;
}
