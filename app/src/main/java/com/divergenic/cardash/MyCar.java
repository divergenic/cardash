package com.divergenic.cardash;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.lang.Math;



/**
 * Created by dependency_injection on 3/5/15.
 */
public class MyCar implements LocationListener {
    private static final int SIGNIFICANT_TIME = 1000 * 60 * 2;
    private static MyCar myCar = null;
    public static double FAR_DISTANCE = 100;
    public static int FAR_INTERVAL = 2000; //in millis
    public static int CLOSE_INTERVAL = 1000; //in millis
    public static int MONITOR_INTERVAL = 500; //in millis
    public static int UPLOAD_INTERVAL = 1000; //in millis
    public static float FAST_ENOUGH = 10.0f;
    public static int SAFETY_MARGIN_DISTANCE = 30;
    public static int NO_CARS_TO_MONITOR = 5;
    public static int CAR_TIMEOUT_MINS = 1; //value in mins
    private static int STATUS_UPDATE_INTERVAL = 1000;
    private static final int SIGNIFICANT_DISTANCE_CHANGE = 2; //in meters
    private final int ALERT_WAIT_INTERVAL = 10000; //in milliseconds

    private final Context context;
    private final String RED = "#ff3333";
    private final String ORANGE = "#ffcc00";
    private final String GREEN = "#a6ca38";
    public double currentSpeed = 0;
    boolean canGetLocation = false;
    private String id = null;
    private ParseObject myParseObject = null;
    private boolean isGPSEnabled = false;
    private boolean isNetworkEnabled = false;
    private LocationManager locationManager;
    private String provider;
    private Location location;
    private ParseGeoPoint parseGeoPoint;
    private Set<Car> closeCars;
    private List<Car> nearestCars;
    private Timer farTimer, closeTimer, nearestTimer;
    private String color = "#0000FF";
    private boolean foundfirstlocation = false;
    private MediaPlayer mp;
    private boolean existingUser, checkedForUser = false;
    private boolean isOK =false;
    private SharedPreferences.Editor edit;
    private SharedPreferences sp;
    private TimerTask farTimerTask, closeTimerTask, nearestTimerTask;
    private boolean scanRunning = false;
    private Status appStatus;
    private boolean savedUser = false;
    private Timer statusTimer;
    private int status;
    private AlertType alertType = AlertType.NONE;
    private double mySafeRadius;
    private boolean isAlertPlaying = false;
    private long lastAlertTime = new Date().getTime();





    private MyCar(Context context) {

        this.context = context;

        closeCars = new HashSet<>();
        nearestCars = new ArrayList<>();
        isOK = CarApplication.isOK();
        //load up settings from shared prefs
        sp = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        edit = sp.edit();
        appStatus = Status.CREATED;
        statusChecker();
        init();
    }

    public static MyCar getInstance(Context context) {

        if(myCar == null)
        {
            myCar = new MyCar(context);

        }
        return myCar;
    }

    public boolean isAlertPlaying() {
        return isAlertPlaying;
    }

    public void setIsAlertPlaying(boolean isAlertPlaying) {
        this.isAlertPlaying = isAlertPlaying;
    }

    public AlertType getAlertType() {
        return this.alertType;
    }

    public void clearAlert() {
        this.alertType = AlertType.NONE;
    }

    private void readPrefValues() {
        FAR_DISTANCE = sp.getInt("VehicleScanRadius", 100);
        FAR_INTERVAL = 10000;//sp.getInt("DataDownloadInterval", 10000);
        UPLOAD_INTERVAL = 2000;//sp.getInt("DataUploadInterval", 3000);
        FAST_ENOUGH = sp.getFloat("SpeedThreshold", 10f); //value in m/s approx equal to 32km/h
        SAFETY_MARGIN_DISTANCE = sp.getInt("SafetyMarginDistance", 30);
        NO_CARS_TO_MONITOR = sp.getInt("CarsToMonitor", 5);
    }

    public void destroyUserCar()
    {
        if(myParseObject != null)
        {
            try {
                myParseObject.delete();

            }
            catch (ParseException pe)
            {
                Log.e("Delete Stack trace", "",pe);
            }
        }
    }

    private void init()
    {

        readPrefValues();
        setAppStatus(Status.READ_PREF_VALUES);

        try {
            myParseObject = ParseUser.getCurrentUser().getRelation("car").getQuery().getFirst();
            myParseObject.put("online", true);
            myParseObject.saveInBackground();
            checkedForUser = true;
            existingUser = true;
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("CarDash: Init user", "",e);
        }

        startScan();

        /*
        //existing user check
        String id_str = sp.getString("ObjectId", null);
        if (id_str!=null)
        {
            isOK = CarApplication.isOK();
            if(isOK)
            {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Car");
                query.getInBackground(id_str, new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        checkedForUser = true;
                        setAppStatus(Status.INITIALISED);
                        if(e == null)
                        {
                            myParseObject = parseObject;
                            id = myParseObject.getObjectId();
                            existingUser = true;
                        }
                        else
                        {
                            switch (e.getCode())
                            {
                                case ParseException.CONNECTION_FAILED: //check network status and update ui accordingly
                                    break;
                                case ParseException.NOT_INITIALIZED: //seems like parse initialise was not called do something
                                    break;
                            }
                        }
                    }
                });

            }

        }
        else
        {
            checkedForUser = true; //now we know we don't have a user...
            setAppStatus(Status.INITIALISED);
        }
        */
    }

    public void reinit()
    {
        readPrefValues();
        setAppStatus(Status.READ_PREF_VALUES);
        stopScan();
        isOK = CarApplication.isOK();
        if(isOK)
        {
            startScan();
        }

    }

    public void stopScan()
    {
        if(scanRunning)
        {
            farTimerTask.cancel();
            closeTimerTask.cancel();
            nearestTimer.cancel();
            scanRunning = false;
            setAppStatus(Status.SCAN_STOPPED);
        }

    }

    public void statusChecker()
    {
        statusTimer = new Timer();
        TimerTask statusTask = new TimerTask() {
            @Override
            public void run() {
                publishUpdate(appStatus);
            }
        };
        statusTimer.schedule(statusTask, 0, STATUS_UPDATE_INTERVAL);
    }

    public void publishUpdate(Status status){
        /*
        MainActivity currentActivity = (MainActivity)context;
        Log.i("PublishUpdate", ""+status);
        switch (status){
            case CREATED: currentActivity.publishUpdate("My Car created");
                          break;
            case READ_PREF_VALUES: currentActivity.publishUpdate("Settings from storage read");
                               break;
            case INITIALISED: currentActivity.publishUpdate("Initialised");
                               break;
            case NETWORKGPSOK: currentActivity.publishUpdate("Network and GPS Checked and Ok");
                                break;
            case SCAN_STARTED: currentActivity.publishUpdate("Scan started");
                                break;
            case FOUND_FIRST_LOCATION: currentActivity.publishUpdate("Found first location");
                                break;
            case SAVED_USER: currentActivity.publishUpdate("Saved user to Parse");
                                break;
            case SCAN_RUNNING: currentActivity.publishUpdate("Scan Running");
                                break;
            case SCAN_STOPPED: currentActivity.publishUpdate("Scan stopped");
                                break;

        }
        */
    }



    public void startScan(){
        synchronized (this) {
            isOK = CarApplication.isOK();
            if(isOK)
            {
                setAppStatus(Status.NETWORKGPSOK);
                AsyncTask as = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        try {
                            while(true)
                            {
                                if(checkedForUser) { //the application will be fully ready if it has also check for existing user in parse

                                    break;
                                }

                                Thread.sleep(1000);

                            }
                        } catch (InterruptedException e) {
                            Log.e("CarDash: StartScan", "exception", e);
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(Object result){

                        mp = MediaPlayer.create(context, R.raw.right);
                        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        if(location != null)
                        {
                            setAppStatus(com.divergenic.cardash.Status.FOUND_FIRST_LOCATION);
                            checkForCarsClose();
                            findNearestCars();
                            monitorNearestCars();
                            scanRunning = true;

                        }
                        else{
                            location = getLocation();
                        }

                    }

                };
                as.execute();
            }

        }




    }

    private double calculateSafetyRadius(double speed, int safetyMargin)
    {
        return 2*speed + (speed*speed)/13.72931 + safetyMargin;
    }

    public double getBearingTo(double lat1, double long1, double lat2,  double long2){
        double dellong = long1 - long2;
        double deg = Math.atan2(Math.sin(dellong) * Math.cos(lat2),
                Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dellong));
        deg = deg*(180/Math.PI);
        deg = (deg+360) % 360;
        return deg;
    }

//    public String genColor()
//        {
//            String hexColor = "";
//            int safeCount = 10; //try at the most 10 times
//            while(true)
//            {
//                float x = new Random().nextFloat();
//                float r = 0.0f;
//                float g = 0.0f;
//                float b = 1.0f;
//                if (x >= 0.0f && x < 0.2f) {
//                    x = x / 0.2f;
//                    r = 0.0f;
//                    g = x;
//                    b = 1.0f;
//                } else if (x >= 0.2f && x < 0.4f) {
//                    x = (x - 0.2f) / 0.2f;
//                    r = 0.0f;
//                    g = 1.0f;
//                    b = 1.0f - x;
//                } else if (x >= 0.4f && x < 0.6f) {
//                    x = (x - 0.4f) / 0.2f;
//                    r = x - 0.4f;
//                    g = 1.0f;
//                    b = 0.0f;
//                } else if (x >= 0.6f && x < 0.8f) {
//                    x = (x - 0.6f) / 0.2f;
//                    r = 0.7f;
//                    g = 1.0f - x;
//                    b = 0.0f;
//                } else if (x >= 0.8f && x <= 1.0f) {
//                    x = (x - 0.8f) / 0.2f;
//                    r = 0.8f;
//                    g = 0.0f;
//                    b = x;
//                }
//            int intColor = Color.rgb((int) (255 * r), (int) (255 * g), (int) (255 * b));
//            hexColor = String.format("#%06X", (0xFFFFFF & intColor));
//            if(!(hexColor.equals(RED) || hexColor.equals(ORANGE)) || safeCount < 0)
//            {
//                break;
//            }
//            safeCount--;
//
//        }
//
//        return hexColor;
//    }

    public String genColor()
    {
        String hexColor = "";
        int safeCount = 10; //try at the most 10 times

        while(true)
        {
            float saturation = 0.5f;
            float hueValue = 0.95f;
            float hue = new Random().nextFloat() * 360;
            float chroma = hueValue * saturation;
            float huePrime = hue/60;
            float x = chroma*(1 - Math.abs((huePrime % 2) - 1));
            float r = 0f;
            float g = 0f;
            float b = 0f;
            if(huePrime >= 0 && huePrime < 1)
            {
                r = chroma;
                g = x;
                b = 0;
            }
            else if(huePrime >= 1 && huePrime < 2)
            {
                r = x;
                g = chroma;
                b = 0;
            }
            else if(huePrime >= 2 && huePrime < 3)
            {
                r = 0;
                g = chroma;
                b = x;
            }
            else if(huePrime >= 3 && huePrime < 4)
            {
                r = 0;
                g = x;
                b = chroma;
            }
            else if(huePrime >= 4 && huePrime < 5)
            {
                r = x;
                g = 0;
                b = chroma;
            }
            else if(huePrime >= 5 && huePrime < 6)
            {
                r = chroma;
                g = 0;
                b = x;
            }
            float m = hueValue - chroma;
            r = r + m;
            g = g + m;
            b = b + m;

            int intColor = Color.rgb((int) (255 * r), (int) (255 * g), (int) (255 * b));
            hexColor = String.format("#%06X", (0xFFFFFF & intColor));
            if(!(hexColor.equals(RED) || hexColor.equals(ORANGE)) || safeCount < 0)
            {
                break;
            }
            safeCount--;

        }

        return hexColor;
    }

    public void checkForCarsClose(){

        farTimer = new Timer();
        farTimerTask = new TimerTask() {
            @Override
            public void run() {
                isOK = CarApplication.isOK();
                //start checking only when user is saved...
                if(isOK && savedUser && location != null) {


                    Log.i("CarDash: close cars", "size " + closeCars.size());
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Car");
                    query.whereNotEqualTo("objectId", myCar.id); //to make sure the our car does not come in the result
                    query.whereWithinKilometers("location", new ParseGeoPoint(location.getLatitude(), location.getLongitude()), (FAR_DISTANCE * 2) / 1000);
                    Date d = new Date();
                    long time = (CAR_TIMEOUT_MINS * 60 * 1000);
                    Date expirationDate = new Date(d.getTime() - (time));
                    query.whereGreaterThanOrEqualTo("updatedAt", expirationDate);
                    //boolean includeOffline = sp.getBoolean("showOfflineCars", false);

//                    if (!includeOffline) {
//                        Date d = new Date();
//                        long time = (5 * 60 * 1000); // 5 mins
//                        Date expirationDate = new Date(d.getTime() - (time));
//                        query.whereGreaterThanOrEqualTo("updatedAt", expirationDate);
//                    }

                    setAppStatus(Status.LOADING_CARS);
                    query.findInBackground(new FindCallback<ParseObject>() {

                        @Override
                        public void done(List<ParseObject> parseObjects, ParseException e) {

                            if (e == null) {
                                setAppStatus(Status.LOADED_CARS);
                                //Log.i("Check close cars", "i was here " + closeCars.size() + " results = "+parseObjects.size());
                                List<Car> tempCloseCars = new ArrayList<Car>();


                                try {
                                    Location myLocation = new Location(location);
                                    //think of synchronising between threads...not done!!
                                    Log.i("within 2 x range ", "" + parseObjects.size());
                                    for (ParseObject po : parseObjects) {
                                        ParseGeoPoint parseGeoPoint2 = po.getParseGeoPoint("location");

                                        //                                Location location2 = new Location("");
                                        //                                location2.setLatitude(parseGeoPoint2.getLatitude());
                                        //                                location2.setLatitude(parseGeoPoint2.getLongitude());
                                        Double dist = parseGeoPoint.distanceInKilometersTo(parseGeoPoint2) * 1000; // to convert distance into meters

                                        Log.i("CarDash: ccc", "Distance in meters = "+dist + " " + po.getString("displayName"));
                                        if (dist < FAR_DISTANCE) {
                                            Log.i("CarDash: ", "" + po.getString("displayName") + " lat:" + parseGeoPoint2.getLatitude() + " long:" + parseGeoPoint2.getLongitude());
                                            //                                    float degrees = myLocation.bearingTo(location2);
//                                            double degrees = getBearingTo(myLocation.getLatitude(), myLocation.getLongitude(),
//                                                    parseGeoPoint2.getLatitude(), parseGeoPoint2.getLongitude());
//                                            Log.i("CarDash", ">>>>>>>Angle in Degrees = " + degrees);
//                                            Double scaledDist = (dist * 100) / FAR_DISTANCE;
//                                            double angleRadians = (degrees * Math.PI / 180);
//                                            double x = scaledDist * Math.cos(angleRadians);
//                                            double y = scaledDist * Math.sin(angleRadians);
                                            double x = 0, y = 0;
                                            Car c = new Car(po.getObjectId(), parseGeoPoint2.getLatitude(),
                                                    parseGeoPoint2.getLongitude(), po.getDouble("speed"), dist, po.getInt("safetyMargin"), po, genColor(), x, y);
                                            //tempCloseCars.add(c);
                                            addToCloseCars(c);


                                            //do gui update...
                                        } else {
                                            Log.i("CarDash: ", "Car is too far, don't bother");
                                        }


                                    }


                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                }

                            } else {
                                Log.e("CarDash: Parse Ex", "findInBackground...", e);
                            }


                        }

                    });
                    setAppStatus(Status.SCAN_RUNNING);
                }
            }
        };
        farTimer.schedule(farTimerTask, 0, FAR_INTERVAL);

    }

    public void findNearestCars(){

        closeTimer = new Timer();
        closeTimerTask = new TimerTask() {
            @Override
            public void run() {
                final List<Car> tempList = new ArrayList<>();

                int closeSize = closeCars.size();
                if(closeSize == 0)
                {
                    synchronized (nearestCars)
                    {
                        nearestCars.clear();
                        return;
                    }
                }

                Log.i("CarDash: Find Nearest", "" + closeSize);
                List<Car> closeCarsTemp = new ArrayList<>();
                synchronized (closeCars)
                {
                    closeCarsTemp = new ArrayList<>(closeCars);
                }
                for(final Car c : closeCarsTemp)
                {

                    //Log.e("Updated Date", "" + oldObj.getUpdatedAt().toGMTString());
                    try{
                        ParseObject po = c.getParseObject().fetch();
                        isOK = CarApplication.isOK();
                        if (isOK) {
                            if (location == null) {
                                    return;
                                }
                            Location myLocation = new Location(location);


                            ParseGeoPoint parseGeoPoint2 = po.getParseGeoPoint("location");
                            Double dist = parseGeoPoint.distanceInKilometersTo(parseGeoPoint2) * 1000; // to convert distance into meters
                            c.setLatitude(parseGeoPoint2.getLatitude());
                            c.setLongitude(parseGeoPoint2.getLongitude());
                            c.setSpeed(po.getDouble("speed"));
                            double oldDist = c.getDistanceFromMe();
                            c.setDistanceFromMe(dist);

                            //Log.i("CarDash", ">>>>>>>ParseGeoPoint 2 lat:" + parseGeoPoint2.getLatitude() + " long:" + parseGeoPoint2.getLongitude());
                            //                                    float degrees = myLocation.bearingTo(location2);
                            double degrees = getBearingTo(myLocation.getLatitude(), myLocation.getLongitude(),
                                    parseGeoPoint2.getLatitude(), parseGeoPoint2.getLongitude());
                            //Log.i("CarDash", ">>>>>>>Angle in Degrees = " + degrees);
                            Double scaledDist = (dist * 100) / FAR_DISTANCE;
                            double angleRadians = (degrees * Math.PI / 180);
                            double x = scaledDist * Math.cos(angleRadians);
                            double y = scaledDist * Math.sin(angleRadians);

                            c.setCoordx(x);
                            c.setCoordy(y);

                            Log.i("CarDash: fnc", ">>>>>>>dist from me = " + dist);
                            int size = tempList.size();
                            long updatedAt = c.getParseObject().getUpdatedAt().getTime();
                            long nowDate = new Date().getTime();
                            long timeDiff = (nowDate - updatedAt)/(1000*60); //diff in mins truncated to the nearest minute
                            Log.i("CarDash: Timediff ", timeDiff + "");
                            if (dist > FAR_DISTANCE || timeDiff > CAR_TIMEOUT_MINS) {
                                //c.setColor(GREEN);
                                removeFromCloseCars(c);

                            }
                            else if ( size == 0 || dist < tempList.get(size-1).getDistanceFromMe()) {
                                if(size == 0)
                                {
                                    tempList.add(c);

                                }
                                else
                                {
                                    int i = 0;
                                    for( ; i < size; i++ )
                                    {
                                        if(dist <= tempList.get(i).getDistanceFromMe() )
                                        {
                                            tempList.add(i, c);
                                            if(tempList.size() > NO_CARS_TO_MONITOR)
                                            {

                                                tempList.remove(tempList.size()-1); //remove the last element
                                            }
                                            break;
                                        }
                                    }


                                }

                            }
                            else if(size < NO_CARS_TO_MONITOR)
                            {
                                tempList.add(c);
                            }

                        }

                    }
                    catch(ParseException pe)
                    {
                        Log.e("Parse Exception", "fetching object", pe);
                    }


                }
                synchronized (nearestCars) {
                    nearestCars.clear();
                    nearestCars.addAll(tempList);
                }
                Log.i("CarDash: Nearest cars", "size = " + nearestCars.size());
                Log.i("CarDash: Nearest", "TempList size = " + tempList.size());




            }
        };
        closeTimer.schedule(closeTimerTask, 0, CLOSE_INTERVAL);


    }

    public void monitorNearestCars(){


        nearestTimer = new Timer();
        nearestTimerTask = new TimerTask() {
            @Override
            public void run() {
                //Log.i("MonitorNearest cars", "i was here " + nearestCars.size());
                synchronized (nearestCars)
                {
                    for(final Car c : nearestCars)
                    {
                        final ParseObject oldObj = c.getParseObject();
                        //Log.e("Updated Date", "" + oldObj.getUpdatedAt().toGMTString());
                        c.getParseObject().fetchInBackground(new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject po, ParseException e) {
                                try {


                                    isOK = CarApplication.isOK();
                                    if (isOK) {
                                        if (e == null) {
                                            if (location == null) {
                                                return;
                                            }
                                            Location myLocation = new Location(location);


                                            ParseGeoPoint parseGeoPoint2 = po.getParseGeoPoint("location");
                                            double dist = parseGeoPoint.distanceInKilometersTo(parseGeoPoint2) * 1000; // to convert distance into meters
                                            c.setLatitude(parseGeoPoint2.getLatitude());
                                            c.setLongitude(parseGeoPoint2.getLongitude());
                                            c.setSpeed(po.getDouble("speed"));
                                            double oldDist = c.getDistanceFromMe();
                                            c.setDistanceFromMe(dist);

                                            //Log.i("CarDash", ">>>>>>>ParseGeoPoint 2 lat:" + parseGeoPoint2.getLatitude() + " long:" + parseGeoPoint2.getLongitude());
                                            //                                    float degrees = myLocation.bearingTo(location2);
                                            double degrees = getBearingTo(myLocation.getLatitude(), myLocation.getLongitude(),
                                                    parseGeoPoint2.getLatitude(), parseGeoPoint2.getLongitude());
                                            //Log.i("CarDash", ">>>>>>>Angle in Degrees = " + degrees);
                                            Double scaledDist = (dist * 100) / FAR_DISTANCE;
                                            double angleRadians = (degrees * Math.PI / 180);
                                            double x = scaledDist * Math.cos(angleRadians);
                                            double y = scaledDist * Math.sin(angleRadians);

                                            c.setCoordx(x);
                                            c.setCoordy(y);


                                            double speed = c.getSpeed();


                                            mySafeRadius = calculateSafetyRadius(currentSpeed, SAFETY_MARGIN_DISTANCE);
                                            double otherSafeRadius = calculateSafetyRadius(c.getSpeed(), c.getSafetyMargin());
                                            c.setCalculatedSafetyRadius(otherSafeRadius);
                                            //Log.i("CarDash: ", "MysRadius OtherSRadius = " + mySafeRadius + " " + otherSafeRadius);
                                            //Log.i("CarDash: Dist speed out", "" + c.getName() + " speed: " + speed + " dist: " + dist + " oldDist: " + oldDist);
                                            if (dist <= (mySafeRadius + otherSafeRadius)) {

                                                Log.i("CarDash: Dist speed in", "" + c.getName() + " speed: " + speed + " dist: " + dist + " oldDist: " + oldDist);
                                                int distDelta = (int)oldDist - (int)dist;
                                                Log.i("CarDash: ", "dist delta: " + distDelta);
                                                if ((speed > FAST_ENOUGH) && distDelta > SIGNIFICANT_DISTANCE_CHANGE )  {
                                                    c.setColor(RED); //yes vehicle is dangerous
                                                    Log.i("CarDash: Alert sounded", "" + c.getName() + " speed: " + speed + " dist: " + dist + " oldDist: " + oldDist);
                                                    Log.i("CarDash: Alert param", "" + alertType + " " + isAlertPlaying());
                                                    long now = new Date().getTime();
                                                    long timeDelta = now - lastAlertTime;
                                                    if((alertType == AlertType.NONE) && !isAlertPlaying() && timeDelta > ALERT_WAIT_INTERVAL) //check that no alert is scheduled to play and also that the alert is not already playing..
                                                    {
                                                        lastAlertTime = now;

                                                        if (degrees > 180) {
                                                            alertType = AlertType.RIGHT;
                                                        } else {
                                                            alertType = AlertType.LEFT;
                                                        }
                                                    }

                                                }
                                                else{

                                                    if(!(c.getColor().equals(RED) && isAlertPlaying())) //checks that no alert is playing
                                                    {
                                                        //its ok vehicle is not dangerous, can set its color to orange..
//                                                        c.setColor(ORANGE);
                                                        c.resetColor();
                                                    }

                                                }

                                            }
                                            else{
                                                if(c.getColor().equals(RED))/* || c.getColor().equals(ORANGE)*/
                                                {
                                                    c.resetColor();
                                                }


                                            }

//later on start a timer for each car seperately for better faster monitoring


                                        } else {
                                            Log.e("Parse Exception", "fetch in background...", e);
                                        }
                                    }

                                }catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });

                    }
                }

            }
        };
        nearestTimer.schedule(nearestTimerTask, 0, MONITOR_INTERVAL);


    }



    public Location getLocation() {

        try {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            this.canGetLocation = true;
            // First get location from Network Provider
            if (isNetworkEnabled) {
                if (locationManager != null) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, UPLOAD_INTERVAL, 0, this);
                }
                if (locationManager != null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                }
            }

            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                if (locationManager != null) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPLOAD_INTERVAL, 0, this);
                }
                if (locationManager != null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                }
            }
            setAppStatus(Status.SCAN_STARTED);

        }

        catch (Exception e) {
            Log.e("on location changed", "Exception", e);
        }

        return location;
    }

    public void saveDetails(){
        if(checkedForUser)
        {

            try {
                ParseGeoPoint point = new ParseGeoPoint(location.getLatitude(), location.getLongitude());
                double speed = Math.max(location.getSpeed(), 0);
                currentSpeed = speed;// * 3.2;
                if(speed <= 0 && existingUser)
                {
                    ParseGeoPoint oldPoint = myParseObject.getParseGeoPoint("location");
                    ParseGeoPoint newPoint = point;
                    if (oldPoint != null) {
                        double distTravelled = oldPoint.distanceInKilometersTo(newPoint) * 1000;//convert to meters
                        Date oldDate = myParseObject.getUpdatedAt();
                        Date newDate = new Date();
                        long diff = newDate.getTime() - oldDate.getTime();
                        diff = diff/1000;
                        if(diff != 0 )
                        {

                            speed = Math.max(distTravelled/diff, 0);
                            Log.i("CarDash: ", "My values Dist Time Speed " + distTravelled + " " + diff + " " + speed);
                        } else {
                            speed = 0;
                        }
                    }

                }


                myParseObject.put("speed", speed);
//        Log.e("Saving details..", "Every update");

                parseGeoPoint = point;
                myParseObject.put("location", point);
                int safeMargin = SAFETY_MARGIN_DISTANCE;
                myParseObject.put("safetyMargin", safeMargin);


                myParseObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e == null)
                        {
                            myCar.id = myParseObject.getObjectId();
                            edit.putString("ObjectId", myCar.id);
                            edit.commit();

                            if(!savedUser)
                            {
                                setAppStatus(Status.SAVED_USER);
                                savedUser = true;
                            }


                        }
                        else
                        {
                            Log.e("Parse Exception", "Saving object...", e);
                        }


                    }
                });
            } catch (Exception exc) {
                exc.printStackTrace();
            }


        }


    }





    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > SIGNIFICANT_TIME;
        boolean isSignificantlyOlder = timeDelta < -SIGNIFICANT_TIME;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.i("CarDash: ", getLocationString(location));

        if(this.location == null || !foundfirstlocation)
        {

            setAppStatus(Status.FOUND_FIRST_LOCATION);
            this.location = location;
            saveDetails();
            checkForCarsClose();
            findNearestCars();
            monitorNearestCars();
            foundfirstlocation = true;
            scanRunning = true;
        }
        else{
            boolean checkBetter = isBetterLocation(location, this.location);
            if(checkBetter)
            {
                this.location = location;
                saveDetails();
            }
        }


    }

    private String getLocationString(Location location) {
        StringBuilder builder = new StringBuilder();

        builder.append("Lat, Long = ");

        builder.append(location.getLatitude());
        builder.append(", ");
        builder.append(location.getLongitude());

        builder.append(" | Speed = ");
        builder.append(location.getSpeed());

        builder.append(" | Accuracy = ");
        builder.append(location.getAccuracy());

        builder.append(" | Bearing (degrees) = ");
        builder.append(Math.toDegrees(location.getBearing()));


        return builder.toString();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
//        Log.e("status changed", "I was here...");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.i("provider enabled", "I was here...");
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.i("provider disabled", "I was here...");
    }

    public String getId() {
        return id;
    }

    public ParseObject getMyParseObject() {
        return myParseObject;
    }

    public boolean isCanGetLocation() {
        return canGetLocation;
    }

    public boolean isNetworkEnabled() {
        return isNetworkEnabled;
    }

    public boolean isGPSEnabled() {
        return isGPSEnabled;
    }

    public void addToNearestCars(int i, Car car) {
        synchronized (nearestCars){
            this.nearestCars.add(i, car);
        }

    }

    public void addToCloseCars(Car car) {
        synchronized (closeCars){
            this.closeCars.add(car);
        }

    }

    public void removeFromCarsApproaching(Car car) {
        synchronized (nearestCars){
            this.nearestCars.remove(car);
        }

    }

    public void removeFromCloseCars(Car car) {
        synchronized (closeCars){
            this.closeCars.remove(car);
        }

    }

    public void setAppStatus(Status status)
    {
        synchronized (appStatus){
            this.appStatus = status;
        }
    }

    public String getCurrentAppStatus() {
        switch (appStatus){
            case CREATED: return "Creating User...";
            case READ_PREF_VALUES: return "Loading Settings...";
            case INITIALISED: return "Initializing...";
            case NETWORKGPSOK: return "Network & GPS OK";
            case SCAN_STARTED: return "Scan started";
            case FOUND_FIRST_LOCATION: return "Location found...";
            case SAVED_USER: return"Updating Database...";
            case SCAN_RUNNING: return "Scanning...";
            case SCAN_STOPPED: return "Scan stopped";
            case LOADING_CARS: return "Scanning...";
            case LOADED_CARS: return "Found "+nearestCars.size()+" car(s)";
        }
        return "Unknown";
    }

    public String getLatLongDisplay() {
        StringBuilder builder = new StringBuilder();

        try {
            builder.append("Lat : ");
            builder.append(location.getLatitude());
            builder.append(",    Long : ");
            builder.append(location.getLongitude());
        }catch (Exception e) {
            return "Lat Long Unknown?!";
        }

        return builder.toString();

    }


    public List<Car> getNearestCars() {
        return nearestCars;
    }

    public Set<Car> getCloseCars() {
        return closeCars;
    }

    public String getColor() {
        return color;
    }

    public boolean isScanRunning(){
        return scanRunning;
    }

    public double getMySafeRadius() {
        return mySafeRadius;
    }

    public void setMySafeRadius(double mySafeRadius) {
        this.mySafeRadius = mySafeRadius;
    }

    public void resetSpeed() {
        this.currentSpeed = 0;
        myParseObject.put("speed", this.currentSpeed);
        try {
            myParseObject.save();
        } catch (ParseException e) {
            Log.e("CarDash: ", "Exception saving final 0 speed to parse", e);
        }
    }
}
