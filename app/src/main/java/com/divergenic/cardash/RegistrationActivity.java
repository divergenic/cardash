package com.divergenic.cardash;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseRelation;
import com.parse.ParseUser;


public class RegistrationActivity extends ActionBarActivity {

    private RadioGroup vehicleType;
    private Button btnStart;
    private EditText txtName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        vehicleType = (RadioGroup) findViewById(R.id.grp_vehicle_type);
        txtName = (EditText) findViewById(R.id.txt_name);

        btnStart = (Button) findViewById(R.id.btnStart);



        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String username = txtName.getEditableText().toString();
                username = username.trim();

                ParseUser currentUser = ParseUser.getCurrentUser();
                ParseObject car = new ParseObject("Car");

                if(username.length() > 0) {
                    car.put("displayName", username);
                }

                car.put("location", new ParseGeoPoint(0.0, 0.0));

                car.put("speed", 0.0);

                car.put("accuracy", 0.0);

                car.put("bearing", 0.0);

                car.put("online", true);

                String vehicleTypeStr = null;

                switch (vehicleType.getCheckedRadioButtonId()) {
                    case R.id.btnHatchback:
                        vehicleTypeStr = "hatchback";
                        break;
                    case R.id.btnSUV:
                        vehicleTypeStr = "suv";
                        break;
                    default:
                        vehicleTypeStr = "sedan";
                        break;
                }

                car.put("vehicleType", vehicleTypeStr);

                ParseRelation relation = currentUser.getRelation("car");
                relation.add(car);

                car.saveEventually();
                currentUser.saveEventually();

                Intent intent=new Intent(RegistrationActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();




            }
        });

    }


}
