package com.divergenic.cardash;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseAnalytics;
import com.parse.ParseCrashReporting;
import com.parse.ParseUser;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import org.acra.*;
import org.acra.annotation.*;


@ReportsCrashes(
        formUri = "https://jaishankarh.cloudant.com/acra-cardash/_design/acra-storage/_update/report",
        reportType = org.acra.sender.HttpSender.Type.JSON,
        httpMethod = org.acra.sender.HttpSender.Method.POST,
        formUriBasicAuthLogin="iturplentenstioledgentra",
        formUriBasicAuthPassword="2vtKbjq1FBJmcaWlAIyvR0no",
        customReportContent = {
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION,
                ReportField.PACKAGE_NAME,
                ReportField.REPORT_ID,
                ReportField.BUILD,
                ReportField.STACK_TRACE
        },
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text

)
public class CarApplication extends Application {

    private static boolean GPSEnabled;
    private static boolean NetworkEnabled;
    private static int CHECK_REFRESH_INTERVAL =10000;
    private Timer resourceTimer;


    public static boolean isOK()
    {
        return GPSEnabled && NetworkEnabled;
    }

    public static boolean checkOK(Context context)
    {
        GPSEnabled = getGPSStatus(context);
        NetworkEnabled = getNetworkStatus(context);
        return isOK();
    }

    public static boolean isNetworkEnabled() {
        return NetworkEnabled;
    }



    public static boolean isGPSEnabled() {
        return GPSEnabled;
    }
    public static boolean getGPSStatus(Context  context){
        LocationManager locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        return locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean getNetworkStatus(Context  context){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);


        GPSEnabled = getGPSStatus(getApplicationContext());
        NetworkEnabled = getNetworkStatus(getApplicationContext());


        // Initialize Crash Reporting.
        //ParseCrashReporting.enable(this);

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        // Add your initialization code here
        Parse.initialize(this, "ptZTr9OduaXfSPW3itsR5OjSGmVpzyFlWJVqNEnm", "r55pjCDHtYxiulbpmQZ4QXpz9unFSQwjMauuQ8Dm");



        ParseUser.enableAutomaticUser();
        ParseUser.getCurrentUser().saveInBackground();
        ParseACL defaultACL = new ParseACL();
        // Optionally enable public read access.
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

        enableWriteLogToFile();

        startRefreshLoop();

    }

    private void enableWriteLogToFile()
    {
        String filePath = Environment.getExternalStorageDirectory() + "/logcat.txt";
        try {
            Runtime.getRuntime().exec(new String[]{"logcat", "-f", filePath, "CarDash*:V"});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void startRefreshLoop() {
        //initialise a timer that will run every 10 sec to check for network and gps
        resourceTimer = new Timer();
        resourceTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                checkOK(getApplicationContext());
            }
        }, CHECK_REFRESH_INTERVAL);

    }

    public void stopRefreshLoop() {
        try {
            resourceTimer.cancel();
            resourceTimer = null;
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


}
