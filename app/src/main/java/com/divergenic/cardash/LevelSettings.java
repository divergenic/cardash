package com.divergenic.cardash;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


public class LevelSettings extends ActionBarActivity implements SeekBar.OnSeekBarChangeListener,View.OnClickListener{

    SeekBar dist_radius_seek,cars_to_monitor_seek,speed_thresh_seek, safety_margin_seek; // extra to remove later co

    TextView dist_radius_view,cars_to_monitor_view,speed_thresh_view, safety_margin_view; //l,

    Button submit,cancel;
    //ToggleButton btnOfflineCars;


    int dist_radius,safety_margin_distance,cars_to_monitor,speed_threshold;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_settings);
        pref=getSharedPreferences("Settings", Context.MODE_PRIVATE);
        dist_radius_view=(TextView)findViewById(R.id.vehicle_radius_view);
        safety_margin_view=(TextView)findViewById(R.id.safety_margin_view);
        cars_to_monitor_view=(TextView)findViewById(R.id.cars_to_monitor_view);
        speed_thresh_view=(TextView)findViewById(R.id.speed_threshold_view);
        dist_radius_seek=(SeekBar)findViewById(R.id.vehicle_radius_seek);
        safety_margin_seek=(SeekBar)findViewById(R.id.safety_margin_seek);
        cars_to_monitor_seek=(SeekBar)findViewById(R.id.cars_to_monitor_seek);
        speed_thresh_seek=(SeekBar)findViewById(R.id.speed_thresh_seek);

        dist_radius_seek.setMax(1000-50);
        dist_radius = pref.getInt("VehicleScanRadius", 100);
        dist_radius_seek.setProgress(dist_radius);
        dist_radius_view.setText(String.valueOf(dist_radius));


        safety_margin_seek.setMax(100);
        safety_margin_distance = pref.getInt("SafetyMarginDistance", 30);
        safety_margin_seek.setProgress(safety_margin_distance);
        safety_margin_view.setText(String.valueOf(safety_margin_distance));

        cars_to_monitor_seek.setMax(10 - 1);
        cars_to_monitor = pref.getInt("CarsToMonitor", 5);
        cars_to_monitor_seek.setProgress(cars_to_monitor);
        cars_to_monitor_view.setText(String.valueOf(cars_to_monitor));

        speed_thresh_seek.setMax(50);
        speed_threshold = (int)(pref.getFloat("SpeedThreshold", 10f) * 3.2);
        speed_thresh_seek.setProgress(speed_threshold);
        speed_thresh_view.setText(String.valueOf(speed_threshold));

        dist_radius_seek.setOnSeekBarChangeListener(this);
        safety_margin_seek.setOnSeekBarChangeListener(this);
        cars_to_monitor_seek.setOnSeekBarChangeListener(this);
        speed_thresh_seek.setOnSeekBarChangeListener(this);

        submit=(Button)findViewById(R.id.btnSubmit);
        submit.setOnClickListener(this);
        cancel=(Button)findViewById(R.id.btnCancel);
        cancel.setOnClickListener(this);
//        btnOfflineCars = (ToggleButton) findViewById(R.id.btnShowOfflineCars);
//        btnOfflineCars.setChecked(pref.getBoolean("showOfflineCars", false));
        dist_radius_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInputDialog(LevelSettings.this, view.getId());

            }
        });

        safety_margin_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInputDialog(LevelSettings.this, view.getId());

            }
        });

        cars_to_monitor_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInputDialog(LevelSettings.this, view.getId());

            }
        });

        speed_thresh_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInputDialog(LevelSettings.this, view.getId());

            }
        });


    }



    private void showInputDialog(Context context, final int id)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Value");

// Set up the input
        final EditText input = new EditText(context);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int value = Integer.parseInt(input.getText().toString());
                displayValue(value, id);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void displayValue(int progress, int id)
    {
        switch (id){
            case R.id.vehicle_radius_view:
                if(progress == 0)
                    dist_radius=progress+50;
                else if(progress > 1000)
                    dist_radius=1000;
                else
                    dist_radius=progress;
                dist_radius_view.setText(dist_radius+"");
                dist_radius_seek.setProgress(dist_radius);
                break;
            case R.id.safety_margin_view:
                if(progress > 100)
                    safety_margin_distance=100;
                else
                    safety_margin_distance=progress;
                safety_margin_view.setText(safety_margin_distance+"");
                safety_margin_seek.setProgress(safety_margin_distance);
                break;
            case R.id.cars_to_monitor_view:
                if(progress == 0)
                    cars_to_monitor=progress+1;
                else if(progress > 10)
                    cars_to_monitor = 10;
                else
                    cars_to_monitor = progress;
                cars_to_monitor_view.setText(cars_to_monitor + "");
                cars_to_monitor_seek.setProgress(cars_to_monitor);
                break;
            case R.id.speed_threshold_view:
                if(progress > 50)
                    speed_threshold=50;
                else
                    speed_threshold=progress;
                speed_thresh_view.setText(speed_threshold+"");
                speed_thresh_seek.setProgress(speed_threshold);
                break;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()){
            case R.id.vehicle_radius_seek:
                dist_radius=progress+50;
                break;
            case R.id.safety_margin_seek:
                safety_margin_distance=progress;
                break;
            case R.id.cars_to_monitor_seek:
                cars_to_monitor=progress+1;
                break;
            case R.id.speed_thresh_seek:
                speed_threshold=progress;
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        switch (seekBar.getId()){
            case R.id.vehicle_radius_seek:
                break;
            case R.id.safety_margin_seek:
                break;
            case R.id.cars_to_monitor_seek:
                break;
            case R.id.speed_thresh_seek:
                break;
        }

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        switch (seekBar.getId()){
            case R.id.vehicle_radius_seek:
                dist_radius_view.setText(dist_radius+"");
                break;
            case R.id.safety_margin_seek:
                safety_margin_view.setText(safety_margin_distance+"");
                //safety_margin_distance=0;
                break;
            case R.id.cars_to_monitor_seek:
                cars_to_monitor_view.setText(cars_to_monitor + "");
                //cars_to_monitor=0;
                break;
            case R.id.speed_thresh_seek:
                speed_thresh_view.setText(speed_threshold+"");
                //speed_threshold=0;
                break;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                editor=pref.edit();
                editor.putInt("VehicleScanRadius",dist_radius);
                editor.putInt("SafetyMarginDistance",safety_margin_distance);
                editor.putInt("CarsToMonitor",cars_to_monitor);
                editor.putFloat("SpeedThreshold",speed_threshold/3.2f);
                //editor.putBoolean("showOfflineCars", btnOfflineCars.isChecked());
                editor.commit();
                MyCar.getInstance(this).reinit();
                Toast.makeText(getApplicationContext(),"Settings Saved",Toast.LENGTH_SHORT).show();
                finish();
                break;
            case R.id.btnCancel:
                finish();
        }
    }
}
